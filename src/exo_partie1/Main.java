package exo_partie1;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Triplet<Integer> tri1 = new Triplet<Integer>(2,4,6);		
		System.out.println(tri1);

		Triplet<Double> tri2 = new Triplet<Double>(2.0,4.1,6.9);		
		System.out.println(tri2);
		
		Triplet<Boolean> tri3 = new Triplet<Boolean>(true,false,false);		
		System.out.println(tri3);
		
		Triplet<String> tri4 = new Triplet<String>("mou","pou","dou");		
		System.out.println(tri4);
		
		TripelQ<Integer,Double,Boolean> tri5 = new TripelQ<Integer, Double, Boolean>(1, 2.3, true);
		System.out.println(tri5);
	}

}
