package exo_partie1;

public class TripelQ <T,U,V> {
	
	private T var1;
	private U var2;
	private V var3;
	
	public TripelQ(T var1, U var2, V var3) {
		super();
		this.var1 = var1;
		this.var2 = var2;
		this.var3 = var3;
	}

	@Override
	public String toString() {
		return "TripelQ [var1=" + var1 + ", var2=" + var2 + ", var3=" + var3 + "]";
	}
	

}
