package exo_partie1;

public class Triplet <T> {
	
	private T var1;
	private T var2;
	private T var3;
	
	public Triplet(T var1, T var2, T var3) {
		super();
		this.var1 = var1;
		this.var2 = var2;
		this.var3 = var3;
	}

	public T getVar1() {
		return var1;
	}

	public T getVar2() {
		return var2;
	}

	public T getVar3() {
		return var3;
	}

	@Override
	public String toString() {
		return "Triplet [var1=" + var1 + ", var2=" + var2 + ", var3=" + var3 + "]";
	}
	
	
	
}
