package old;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Exemple<Integer,String> entier = new Exemple<Integer,String>();
		entier.setVar1(10);
		entier.setVar2("Sboob");
		entier.setMonEntier(12);
		System.out.println(entier.getVar1().getClass().getTypeName() + " " + entier.getVar1());
		System.out.println(entier.getVar2().getClass().getTypeName() + " " + entier.getVar2());
		
		Operation<Double> op1 = new Operation<Double>(2.3,5.6);
		Operation<String> op2 = new Operation<String>("bin","go");
		Operation<Integer> op3 = new Operation<Integer>(3,5);
		Operation<Boolean> op4 = new Operation<Boolean>(true,false);
		
		op1.plus();
		op2.plus();
		op3.plus();
		op4.plus();
	}

}
